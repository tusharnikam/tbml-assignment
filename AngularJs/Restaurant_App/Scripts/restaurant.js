var app=angular.module("restaurantApp",[]);


document.addEventListener("DOMContentLoaded", function () {
    const loginModal = document.getElementById("loginModal");
    const signupModal = document.getElementById("signupModal");
    
    const showModal=(modal)=>{
        return new Promise((resolve,reject)=>{
            $(modal).on('shown.bs.modal',() =>{
                resolve();

            }).modal('show');
        });
    };

    const loginButton = document.querySelector(".btn[data-target='#loginModal']");
    const signupButton = document.querySelector(".btn[data-target='#signupModal']");

   
  

    loginButton.addEventListener("click", async()=>{
        await showModal(loginModal);
        console.log("Login modal shown");
    });
    
    signupButton.addEventListener("click", async()=>{
        await showModal(signupModal);
        console.log("signup modal shown");
    });
});